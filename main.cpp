#include <iostream>
#include "DynamicArray.h"
#include "DynamicArrayException.h"

using namespace dynamicArray;
int main() {
        DynamicArray arr1;
    DynamicArray arr2(3);
    DynamicArray arr3(3, 1, 0);
    DynamicArray arr4(4, 2, 2);
    DynamicArray arr5(DynamicArray (3, 3, 0));
    DynamicArray arr6(arr3);
    std::cout << "#1 " << arr1 << std::endl;
    std::cout << "#2 " << arr2 << std::endl;
    std::cout << "#3 " << arr3 << std::endl;
    std::cout << "#4 " << arr4 << std::endl;
    std::cout << "#5 " << arr5 << std::endl;
    std::cout << "#6 " << arr6 << std::endl;
    if(arr6 == arr3) std::cout << "arr6 == arr3? true" << std::endl;
    if(arr2 != arr3) std::cout << "arr2 != arr3? true" << std::endl;
    if(arr3 < arr4) std::cout << "arr3 < arr4? true" << std::endl;
    if(arr5 > arr4) std::cout << "arr5 > arr4? true" << std::endl;
    arr2[0] = 1;
    arr2[1] = 2;
    arr2[2] = 3;
    std::cout << "arr2: " << arr2[0] << ", " << arr2[1] << ", " << arr2[2] << std::endl;
    arr4.resize(10);
    std::cout << "#4 " << arr4 << std::endl;
    arr3.reservation(5);
    std::cout << "#3 " << arr3 << std::endl;
    std::cout << "#6 " << arr6 << std::endl;
    std::cout << "arr2 + arr4: " << arr2 + arr4 << std::endl;
    arr1 = DynamicArray(5, 5, 5);
    std::cout << "#1 " << arr1 << std::endl;
    arr1 = arr2;
    std::cout << "#1 " << arr1 << std::endl;
    arr1.pushBack(11);
    arr1.pushBack(12);
    arr1.pushBack(13);
    std::cout << "#1 " << arr1 << std::endl;
    arr1.popBack();
    arr1.popBack();
    std::cout << "#1 " << arr1 << std::endl;
    try {
        bool flag = arr1 == arr6;
    } catch(DynamicArrayException* e) {
        std::cout << "arr1 == arr6: " << e->getMessage() << std::endl;
    }
    arr1 = DynamicArray();
    std::cout << "#1 " << arr1 << std::endl;
    arr1.popBack();
    try {
        int k = arr1.popBack();
    } catch(DynamicArrayException* e) {
        std::cout << "arr1 popBack: " << e->getMessage() << std::endl;
    }
    try {
        int k = arr1[2];
    } catch(DynamicArrayException* e) {
        std::cout << "arr1[2]: " << e->getMessage() << std::endl;
    }
    std::cout << "<<<< end test >>>>"<< std::endl;
    return 0;

}
