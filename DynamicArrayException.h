#ifndef TASKS_1_DYNAMICARRAYEXCEPTION_H
#define TASKS_1_DYNAMICARRAYEXCEPTION_H
class DynamicArrayException {
private:
    char*message;
public:
    explicit DynamicArrayException(char *message);
    char *getMessage();
};


#endif //TASKS_1_DYNAMICARRAYEXCEPTION_H
