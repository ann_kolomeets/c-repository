#include "DynamicArray.h"
#include "DynamicArrayException.h"
#include <iostream>

dynamicArray::DynamicArray::DynamicArray(){
    array = new int [1];
    size = 1;
    array[0] = 0;
    reserve = 0;
}

dynamicArray::DynamicArray::DynamicArray(unsigned int size){
    array = new int[size];
    this->size = size;
    reserve = 0;
    for (int i = 0; i < size; i++){
        array[i] = 0;
    }
}

dynamicArray::DynamicArray::DynamicArray(unsigned int size, int n, unsigned int reserve){
    array = new int[size];
    this->size = size;
    this->reserve = reserve;
    for (int i = 0; i < size; i++){
        array[i] = n;
    }
    for (int i = size; i < size+ reserve; i++){
        array[i] = 0;
    }
}

dynamicArray::DynamicArray::DynamicArray(const DynamicArray& obj){
    array = new int[obj.size + obj.reserve];
    this->size = obj.size;
    for (int i = 0; i < obj.size; i++){
        array[i] = obj.array[i];
    }
    for(int i = obj.size; i < obj.size+ obj.reserve; i++){
        array[i] = 0;
    }
    this->reserve = obj.reserve;
}

dynamicArray::DynamicArray::DynamicArray(DynamicArray &&obj) noexcept {
    array = obj.array;
    this->reserve = obj.reserve;
    this->size = obj.size;
    obj.array = nullptr;
}

dynamicArray::DynamicArray::~DynamicArray() {
    if(array){
        delete[]array;
        size = 0;
    }
}

unsigned int dynamicArray::DynamicArray::getSize() const {
    return size;
}

int& dynamicArray::DynamicArray::operator[] (int index) {
    if (index >= 0 && index < size){
        return array[index];
    }
    else{
        throw new DynamicArrayException("Invalid index");
    }
}

void dynamicArray::DynamicArray::resize(int newSize) {
    if (newSize < size){
        for (int i = newSize; i < size; i++){
            array[i] = 0;
            reserve++;
        }
    } else {
        int *current = new int[size];
        for (int i = 0; i < size; i++) {
            current[i] = array[i];
        }
        delete[] array;

        array = new int[size + reserve];
        for (int i = 0; i < size; i++) {
            array[i] = current[i];
        }
        for(int i = size; i < size+reserve; i++){
            array[i] = 0;
        }
        delete[] current;
        current = nullptr;
    }
    size = newSize;

}

/*dynamicArray::DynamicArray& dynamicArray::DynamicArray::operator = (const dynamicArray::DynamicArray& obj) {
    delete[] array;
    array = new int[obj.size + obj.reserve];
    for(int i = 0; i < obj.size; i++) {
        array[i] = obj.array[i];
    }
    for(int i = obj.size; i < obj.size + obj.reserve; i++) {
        array[i] = 0;
    }
    this->size = obj.size;
    this->reserve = obj.reserve;
}*/

dynamicArray::DynamicArray& dynamicArray::DynamicArray::operator = (dynamicArray::DynamicArray&& obj) noexcept {
    delete[] array;
    array = obj.array;
    this->size = obj.size;
    this->reserve = obj.reserve;
    obj.array = nullptr;
}

unsigned int dynamicArray::DynamicArray::getCapacity() {
    return reserve;
}

bool dynamicArray::DynamicArray::operator==(const dynamicArray::DynamicArray &obj) const {
    if (size == obj.size) {
        for (int i = 0; i < size; i++) {
            if (array[i] != obj.array[i]) {
                return false;
            }
        }
        return true;
    } else {
        throw new DynamicArrayException("Different size of array");
    }
}

bool dynamicArray::DynamicArray::operator !=(const dynamicArray::DynamicArray& obj) const {
    try {
        return !(*this == obj);
    } catch(DynamicArrayException e) {
        throw e;
    }
}

bool dynamicArray::DynamicArray::operator<=(const dynamicArray::DynamicArray &obj) const {
    return compare(array, obj.array, size, obj.size) <= 0;
}

bool dynamicArray::DynamicArray::operator >= (const dynamicArray::DynamicArray& obj) const {
    return compare(array, obj.array, size, obj.size) >= 0;
}

bool dynamicArray::DynamicArray::operator < (const dynamicArray::DynamicArray& obj) const {
    return compare(array, obj.array, size, obj.size) < 0;
}

bool dynamicArray::DynamicArray::operator > (const dynamicArray::DynamicArray& obj) const {
    return compare(array, obj.array, size, obj.size) > 0;
}

dynamicArray::DynamicArray &dynamicArray::DynamicArray::operator+(const dynamicArray::DynamicArray &obj) {
    DynamicArray *current = new DynamicArray(size+ obj.size, 0 , reserve + obj.reserve);

    for (int i = 0; i < size; i++){
        current->array[i] = obj.array[i];
    }

    for (int i = size; i < size+obj.size; i++){
        current->array[i] = array[i - size];
    }
    for (int i = size+obj.size; i < size+obj.size+reserve+obj.reserve; i++) {
        current->array[i] = 0;
    }
    return *current;
}


void dynamicArray::DynamicArray::reservation(unsigned int newRes) {
    int* current = new int[size];
    for(int i = 0; i < size; i++) {
        current[i] = array[i];
    }
    delete[] array;

    array = new int[size + newRes];
    for(int i = 0; i < size; i++) {
        array[i] = current[i];
    }

    for(int i = size; i < size + newRes; i++) {
        array[i] = 0;
    }
    delete[] current;
    current = nullptr;
    reserve = newRes;
}

std::istream &dynamicArray::operator>>(std::istream &s, dynamicArray::DynamicArray &dynamicArray) {
    int n;
    std::cout << "Size: ";
    s >> n;
    dynamicArray.resize(dynamicArray.size + n);
    std::cout << "\nElements: ";
    for(int i = dynamicArray.size; i < dynamicArray.size + n; i++) {
        s >> dynamicArray[i];
    }
    return s;
}

std::ostream &dynamicArray::operator<<(std::ostream &s, const dynamicArray::DynamicArray &dynamicArray) {
    s << "size = " << dynamicArray.size << ", capacity = " << dynamicArray.reserve << " {";
    if(dynamicArray.size > 0) {
        for(int i = 0; i < dynamicArray.size - 1; i++) {
            s << dynamicArray.array[i] << ", ";
        }
        s << dynamicArray.array[dynamicArray.size - 1] << "}";
    } else {
        s << "empty";
    }
    return s;
}

void dynamicArray::DynamicArray::pushBack(int element) {
    if (reserve > 0){
        array[size] = element;
        reserve --;
    } else{
        int *current = new int[size];
        for (int i = 0; i < size; i++){
            current[i] = array[i];
        }
        delete[]  array;

        array = new int[size + 1];
        for (int i = 0; i<size; i++){
            array[i] = current[i];
        }
        array[size] = element;
        delete [] current;
        current = nullptr;
    }
    size++;
}
int dynamicArray::DynamicArray::popBack(){
    if (size > 0){
        int result = array[size - 1];
        reserve ++;
        size --;
        array[size] = 0;
        return result;
    } else {
        throw new DynamicArrayException("Size < 0");
    }
}

int dynamicArray::compare(int* arr1, int* arr2, int size1, int size2){
    int size;
    if(size1 == size2) size = size1;
    else if(size1 < size2) size = size1;
    else size = size2;
    for(int i = 0; i < size; i++) {
        if(arr1[i] > arr2[i]) {
            return 1;
        } else if(arr1[i] < arr2[i]) {
            return -1;
        }
    }
    if(size1 == size2) return 0;
    else if(size1 < size2) return -1;
    else return 1;
}


