
#ifndef TASKS_1_DYNAMICARRAY_H
#define TASKS_1_DYNAMICARRAY_H

#include <iostream>
namespace dynamicArray {
    class DynamicArray {
    private:
        int *array;
        unsigned int size;
        unsigned int reserve;

    public:
        DynamicArray();

        explicit DynamicArray(unsigned int size);

        DynamicArray(unsigned int size, int n, unsigned int reserve);

        DynamicArray(const DynamicArray &obj);

        DynamicArray(DynamicArray &&obj) noexcept;

        ~DynamicArray();

        unsigned int getSize() const;

        int& operator [](int index);

        void resize(int newSize);

        //DynamicArray& operator =(const DynamicArray &obj);

        DynamicArray& operator = (dynamicArray::DynamicArray&& obj) noexcept;

        unsigned int getCapacity();

        bool operator == (const DynamicArray& obj) const;

        bool operator != (const DynamicArray& obj) const;

        bool operator <= (const DynamicArray& obj) const;

        bool operator >= (const DynamicArray& obj) const;

        bool operator < (const DynamicArray& obj) const;

        bool operator > (const DynamicArray& obj) const;

        DynamicArray& operator + (const DynamicArray& obj);

        void reservation(unsigned int newRes);

        friend std::istream& operator >> (std::istream& s, DynamicArray& dynamicArray);
        friend std::ostream& operator << (std::ostream& s, const DynamicArray& dynamicArray);

        void pushBack(int element);
        int popBack();
    };

    int compare(int *arr1, int *arr2, int size1, int size2);

}
#endif //TASKS_1_DYNAMICARRAY_H
